
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');

var dir = {
    assets: './app/Resources/',
    dist: './web/',
    node: './node_modules/assets/',
    bower: './bower_components/'
};

gulp.task('sass', function () {
    gulp.src(dir.assets + 'css/*.sass')
        .pipe(sass())
        .pipe(uglifycss())
        .pipe(gulp.dest(dir.dist + 'css'));
});

gulp.task('css', function() {
    gulp.src(dir.assets + 'css/*.css')
        .pipe(uglifycss())
        .pipe(gulp.dest(dir.dist + 'css'));
});

gulp.task('js',function()
{
    gulp.src(dir.assets + 'js/*.js')
        .pipe(uglify({ mangle: false }))
        .pipe(gulp.dest(dir.dist +'js'));
});


gulp.task('assets',function()
{
    gulp.src(dir.node + 'bootstrap/dist/css/bootstrap.min.css')
        .pipe(gulp.dest(dir.dist +'assets'));
    gulp.src(dir.node + 'bootstrap/dist/js/bootstrap.min.js')
        .pipe(gulp.dest(dir.dist +'assets'));
    gulp.src(dir.node + 'jquery/dist/jquery.min.js')
        .pipe(gulp.dest(dir.dist + 'assets'));
    gulp.src(dir.node + 'angular/angular.min.js')
        .pipe(gulp.dest(dir.dist + 'assets'));

    gulp.src(dir.bower + 'bootstrap/dist/css/bootstrap.min.css')
        .pipe(gulp.dest(dir.dist +'assets'));
    gulp.src(dir.bower + 'bootstrap/dist/js/bootstrap.min.js')
        .pipe(gulp.dest(dir.dist +'assets'));
    gulp.src(dir.bower + 'jquery/dist/jquery.min.js')
        .pipe(gulp.dest(dir.dist + 'assets'));
    gulp.src(dir.bower + 'angular/angular.min.js')
        .pipe(gulp.dest(dir.dist + 'assets'));
    
});

gulp.task('fonts',function()
{
    gulp.src(dir.node +'bootstrap/fonts/glyphicons-halflings-regular.*')
        .pipe(gulp.dest(dir.dist +'fonts'));
    gulp.src(dir.bower +'bootstrap/fonts/glyphicons-halflings-regular.*')
        .pipe(gulp.dest(dir.dist +'fonts'));
    gulp.src(dir.assets + 'fonts/*.ttf')
        .pipe(gulp.dest(dir.dist +'fonts'));
});

gulp.task('images', function()
{
    gulp.src(dir.assets + 'images/**/*')
        .pipe(gulp.dest(dir.dist + 'images'));
});

gulp.task('default', ['sass', 'css', 'js', 'assets','fonts','images']);
