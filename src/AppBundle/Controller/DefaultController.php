<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Type\MultiplayerServer;
use AppBundle\Entity\Statistics\GameStatistics;
use AppBundle\Entity\Vote;
use AppBundle\Form\ServerType;
use AppBundle\Form\VoteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $util = $this->get('util');
        $stats = $this->getDoctrine()->getRepository('AppBundle:Statistics\GameStatistics')->findMostPopular(15);

        //[GameName][0||1][ServerId]
        $topServers = [];
        for ($i = 0; $i<count($stats); $i++) {
            $s = $stats[$i];
            $topServers[$s->getName()][0] = $util->getServersByVotes($s->getName(), 3);
            if (count($topServers[$s->getName()][0])<=0) {
                continue;
            }
            if ($topServers[$s->getName()][0][0] instanceof MultiplayerServer) {
                $topServers[$s->getName()][1] = $util->getServersByPlayersOn($s->getName(), 3);
            }
        }

        return $this->render('default/index.html.twig', [
            'games' => $stats,
            'topServers' => $topServers
        ]);
    }
    public function leftSidebarAction()
    {
        $stats = $this->getDoctrine()->getRepository('AppBundle:Statistics\GameStatistics')->findMostPopular(9);
        return $this->render('default/leftSidebar.html.twig', [
            'games' => $stats
        ]);
    }
    public function gameIndexAction($game)
    {
        $g = $this->getDoctrine()->getRepository('AppBundle:Statistics\GameStatistics')->findBy(['name'=>$game], [], 1);
        if (count($g) == 0) {
            return $this->redirectToRoute('app.index');
        }
        return $this->render('default/servers.html.twig', [
            'gameObject' => $g[0],
            'game' => $game
        ]);
    }
    public function passChangeAction()
    {
        if ($this->getUser()==null) {
            return $this->redirectToRoute('app.index');
        }
        return $this->render('user/profile.html.twig', [
            'user' => $this->getUser()
        ]);
    }
    public function userProfileAction(Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($id);
        if ($user==null) {
            return $this->redirectToRoute('app.index');
        }
        $owner = false;
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.change_password.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);
        $formView = null;
        if ($this->getUser()==$user) {
            $formView = $form->createView();
            $owner = true;
        }
        return $this->render('user/profile.html.twig', [
            'user' => $user,
            'form' => $formView,
            'owner' => $owner
        ]);
    }
    public function profileAction(Request $request)
    {
        if ($this->getUser()==null) {
            return $this->redirectToRoute('app.index');
        }
        $id = $this->getUser()->getId();
        return $this->userProfileAction($request, $id);
    }
    public function serverInfoAction($game, $id)
    {
        try {
            $g = $this->get('util')->getServer($game, $id);
        } catch (\Exception $e) {
            return $this->redirectToRoute('app.index');
        }
        if ($g==null) {
            return $this->redirectToRoute('app.gameInfo', ['game' => $game]);
        }
        return $this->render(':default:server.html.twig', [
            'server' => $g,
            'game' => $game
        ]);
    }
    public function voteAction(Request $request, $game, $id)
    {
        $util = $this->get("util");
        $server = $util->getServer($game, $id);
        $form = null;
        $voted = false;
        $timeLeft = $util->timeLeftToVote($request->getClientIp(), $server);
        if ($timeLeft==0) {
            $vote = new Vote($server->getRating());
            $form = $this->createForm(VoteType::class, $vote, ['ip' => $request->getClientIp()]);
            $form ->handleRequest($request);
            if (!$form->isSubmitted()) {
                $message = "";
            } elseif ($form->isValid()) {
                $voted = true;
                $message = 'CAPTCHA patvirtintas, balsas sėkmingai įskaičiuotas!';
                $server->getGameStatistics()->addTotalVotes();
                $this->getDoctrine()->getManager()->persist($vote);
                $server->getRating()->setTotalVotes($server->getRating()->getTotalVotes()+1);
                $this->getDoctrine()->getManager()->persist($server->getGameStatistics());
                $this->getDoctrine()->getManager()->flush();
            } else {
                $message = 'CAPTCHA neteisingas, bandykite dar kartą!';
            }
        } else {
            $m = $timeLeft/60 | 0;
            $h = $m/60 | 0;
            $message = 'Vėl balsuoti galėsite tik po ';
            $message .= $h>0?$h.' val. ':'';
            $message .= $m>0?($m-$h*60).' min. ':'';
            $message .= ($timeLeft-$m*60) . ' sek.';
        }

        return $this->render(':default:vote.html.twig', array(
            'form' => ($form==null || $voted)?null:$form->createView(),
            'message' => $message,
            'game' => $game,
            'id' => $id
        ));
    }
    public function gameChooseAction()
    {
        if ($this->getUser()==null) {
            return $this->redirectToRoute('app.index');
        }
        $games = $this->getDoctrine()->getRepository('AppBundle:Statistics\GameStatistics')->findAll();
        return $this->render(':user:chooseServer.html.twig', array(
            'games' => $games
        ));
    }
    public function serverSubmitAction(Request $request, $game)
    {
        if ($this->getUser()==null) {
            return $this->redirectToRoute('app.index');
        }
        $m = $this->getDoctrine()->getManager();
        $util = $this->get("util");
        $server = $util->createGameServer($game, $this->getUser(), new Rating());

        $form = $this->createForm(ServerType::class, $server, ['game' => $game]);
        $form ->handleRequest($request);
        $stats = $util->getGameStatistics($game);
        if ($form->isValid()) {
            $rating = new Rating();
            $server->setRating($rating);
            $server->setGameStatistics($stats);
            $stats->addTotalServers(true);
            $m->persist($stats);
            $m->persist($rating);
            $m->persist($server);
            $m->flush();
            $this->get('game_query')->updateQuery($server);
            return $this->redirectToRoute('app.serverInfo', ['game' => $game, 'id' => $server->getId()]);
        }
        return $this->render(':user:submitServer.html.twig', array(
            'form' => ($form==null)?null:$form->createView(),
            'game' => $stats
        ));
    }
    public function serverEditAction(Request $request, $game, $id)
    {
        $m = $this->getDoctrine()->getManager();
        $util = $this->get("util");
        $server = $util->getServer($game, $id);
        if ($server->getOwner()!=$this->getUser()) {
            return $this->redirectToRoute('app.index');
        }
        $form = $this->createForm(ServerType::class, $server, ['game' => $game]);
        $form ->handleRequest($request);

        if ($form->isValid()) {
            $m->persist($server);
            $m->flush();
            return $this->redirectToRoute('app.serverInfo', ['game' => $game, 'id' => $server->getId()]);
        }
        return $this->render(':user:editServer.html.twig', array(
            'form' => ($form==null)?null:$form->createView()
        ));
    }
}
