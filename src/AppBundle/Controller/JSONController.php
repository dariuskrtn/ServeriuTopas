<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JSONController extends Controller
{
    public function serverInformationAction(Request $request, $game, $id)
    {
        $manager = $this->getDoctrine()->getManager();
        $server = $this->get('util')->getServer($game, $id);
        if ($request->query->get("update") == "true") {
            $server = $this->get('game_query')->updateQuery($server);
            $manager->persist($server);
            $manager->flush();
        }
        return new Response(json_encode($server));
    }

    public function allServersAction(Request $request, $game)
    {
        $order = "id";
        if ($request->query->get("order") != null) {
            $order = $request->query->get("order");
        }
        $servers = $this->get('util')->getServersByVotes($game, 50);
        return new Response(json_encode($servers));
    }

    public function serverRatingsAction($game, $id)
    {
        $manager = $this->getDoctrine()->getManager();
        $server = $manager->getRepository("AppBundle:Server\\" . $game . "Server")->find($id);
        return new Response(json_encode($server->getRating()));
    }
    public function userServersAction($id)
    {
        $user = $this->getDoctrine()->getManager()->getRepository('AppBundle:User')->find($id);
        //$user = $this->getUser();
        if ($user==null) {
            return new Response('');
        }
        return new Response(json_encode($this->get('util')->getUserServers($user)));
    }
    public function serverRemoveAction(Request $request, $game, $id)
    {
        if ($this->getUser()==null) {
            return new Response('');
        }
        $m = $this->getDoctrine()->getManager();
        $server = $this->get('util')->getServer($game, $id);
        if ($server==null || $server->getOwner()!=$this->getUser()) {
            return new Response('');
        }
        $stats = $server->getGameStatistics();
        $stats->addTotalServers(false);
        $m->persist($stats);
        $m->remove($server);
        $m->flush();
        return new Response('OK');
    }
    public function rateAction(Request $request, $game, $id)
    {
        $m = $this->getDoctrine()->getManager();
        if ($this->getUser()==null || $request->query->has('ratings')) {
            return new Response("No permissions");
        }
        $server = $this->get('util')->getServer($game, $id);
        $server->getRating()->setRatings($request->query->get('ratings'));
        $m->persist($server->getRating());
        $m->flush();
        return new Response("OK");
    }
    public function getVotesAction($game, $id)
    {
        $s = $this->get('util')->getServer($game, $id);
        return new Response($s->getRating()->getTotalVotes().'');
    }
}
