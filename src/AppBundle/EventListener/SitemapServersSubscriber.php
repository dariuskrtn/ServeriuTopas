<?php
/**
 * Created by PhpStorm.
 * User: darius0021
 * Date: 16.11.4
 * Time: 12.58
 */

namespace AppBundle\EventListener;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;

class SitemapServersSubscriber implements EventSubscriberInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @param UrlGeneratorInterface $urlGenerator
     * @param ObjectManager         $manager
     */
    public function __construct(UrlGeneratorInterface $urlGenerator, ObjectManager $manager)
    {
        $this->urlGenerator = $urlGenerator;
        $this->manager = $manager;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            SitemapPopulateEvent::ON_SITEMAP_POPULATE => 'registerServersPages',
        ];
    }

    /**
     * @param SitemapPopulateEvent $event
     */
    public function registerServersPages(SitemapPopulateEvent $event)
    {
        $games = $this->manager->getRepository('AppBundle:Statistics\GameStatistics')->findAll();
        foreach ($games as $g) {
            $event->getUrlContainer()->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'app.gameInfo',
                        ['game' => $g->getName()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ),
                'games'
            );
        }
        foreach ($games as $g) {
            $servers = $this->manager->getRepository('AppBundle:Server\\'.$g->getName().'Server')->findAll();
            foreach ($servers as $s) {
                $event->getUrlContainer()->addUrl(
                    new UrlConcrete(
                        $this->urlGenerator->generate(
                            'app.serverInfo',
                            ['game' => $g->getName(), 'id' => $s->getId()],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        )
                    ),
                    'servers'
                );
            }
        }
        $users = $this->manager->getRepository('AppBundle:User')->findAll();
        foreach ($users as $user) {
            $event->getUrlContainer()->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'app.userProfile',
                        ['id' => $user->getId()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                ),
                'users'
            );
        }
    }
}