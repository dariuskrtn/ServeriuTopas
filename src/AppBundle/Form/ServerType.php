<?php

namespace AppBundle\Form;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Class WorkoutType
 * @package AppBundle\Form
 */
class ServerType extends AbstractType
{
    const STYLES = [
        'Minecraft' => ['RPG'=>'RPG', 'Survival'=>'Survival', 'Minigames'=>'Minigames',
            'Creative'=>'Creative', 'PVP'=>'PVP', 'Prison'=>'Prison', 'Faction'=>'Faction',
            'Skyblock'=>'Skyblock', 'Hardcore'=>'Hardcore', 'Economy'=>'Economy',
            'BungeeCord'=>'BungeeCord', 'Kita'=>'Kita'],
        'CounterStrike16' => ["Public"=>"Public", "CSDM"=>"CSDM", "GunGame"=>"GunGame",
        'Deathrun'=>'Deathrun', 'JailBreak'=>'JailBreak', 'Hide&Seek'=>'Hide&Seek',
        'Surf'=>'Surf', 'Zombie'=>'Zombie', 'Warcraft'=>'Warcraft', 'SuperHero'=>'SuperHero',
        'Knife'=>'Knife', 'Kita'=>'Kita']
    ];
    const GAME_OPTIONS = [
        'Minecraft' => [
            'version' => ['version', TextType::class, ['required'=>1]],
            'style' => ['style', ChoiceType::class,
                array('choices' => self::STYLES['Minecraft'])]
        ],
        'CounterStrike16' => [
            'style' => ['style', ChoiceType::class,
                array('choices' => self::STYLES['CounterStrike16'])]
        ],
        'CounterStrikeCZ' => [
            'style' => ['style', ChoiceType::class,
                array('choices' => self::STYLES['CounterStrike16'])]
        ],
        'CounterStrikeGO' => [
            'style' => ['style', ChoiceType::class,
                array('choices' => self::STYLES['CounterStrike16'])]
        ],
        'CounterStrikeS' => [
            'style' => ['style', ChoiceType::class,
                array('choices' => self::STYLES['CounterStrike16'])]
        ],
        'SAMP' => [],
        'Rust' => [],
        'KillingFloor2' => [],
        'KillingFloor' => [
            'queryPort' => ['queryPort', TextType::class,
                array('required' => 0)]
        ],
        'TeamFortress2' => [],
        'Unturned' => []
    ];
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Server\Type\BaseServer',
            'game' => 'Minecraft'
        ));
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('hostname', TextType::class)
            ->add('port', TextType::class)
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('webpage', TextType::class, array('required' => false));
        foreach (self::GAME_OPTIONS[$options['game']] as $opt) {
            if (count($opt)==2) {
                $builder->add($opt[0], $opt[1]);
            } else {
                $builder->add($opt[0], $opt[1], $opt[2]);
            }
        }
            $builder->add('recaptcha', EWZRecaptchaType::class, array(
                'attr' => array(
                    'options' => array(
                        'theme' => 'light',
                        'type'  => 'image',
                        'size'  => 'normal',
                        'defer' => true,
                        'async' => true
                    )
                )
            ))
            ->add('save', SubmitType::class, array('label' => 'Išsaugoti'))
            ->getForm();
    }
}
