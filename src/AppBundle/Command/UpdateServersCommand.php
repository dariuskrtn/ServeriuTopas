<?php
/**
 * Created by PhpStorm.
 * User: darius0021
 * Date: 16.10.25
 * Time: 17.32
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateServersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('servers:update')

            // the short description shown while running "php bin/console list"
            ->setDescription('Updates servers information.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Ne cia pataikei")
            ->addArgument('option', InputArgument::REQUIRED, 'oldest;')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // ...
        if (!$input->hasArgument('option')) {
            $output->writeln('Missing option. choose oldest.');
            return;
        }
        if ($input->getArgument('option')=='oldest') {
            $output->writeln('Updating.');
            $util = $this->getContainer()->get('util');
            $gq = $this->getContainer()->get('game_query');
            $m = $this->getContainer()->get('doctrine')->getManager();
            $games = $m->getRepository('AppBundle:Statistics\GameStatistics')->findAll();
            foreach ($games as $g) {
                try {
                    $oldest = $util->getOldestUpdated($g->getName(), 2);
                    if ($oldest==null) {
                        //$output->writeln("null");
                        continue;
                    }
                    foreach ($oldest as $old) {
                        try {
                            $gq->updateQuery($old);
                            //$output->writeln($old->getName());
                            $m->persist($old);
                        } catch (\Exception $e) {
                        }
                    }
                } catch (\Exception $e) {
                    //$output->writeln($e->getTraceAsString());
                }
            }
            $m->flush();
            $output->writeln('Oldest servers updated.');
            return;
        }
        $output->writeln('Option not found.');
    }
}