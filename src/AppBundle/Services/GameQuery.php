<?php
/**
 * Created by PhpStorm.
 * User: darius0021
 * Date: 16.9.11
 * Time: 16.42
 */

namespace AppBundle\Services;

use AppBundle\Entity\Server\KillingFloorServer;
use AppBundle\Entity\Server\MinecraftServer;
use AppBundle\Entity\Server\Type\BaseServer;
use AppBundle\Entity\Server\Type\MultiplayerServer;
use AppBundle\Services\Minecraft\MinecraftPing;
use GameQ\GameQ;

class GameQuery
{
    /**
     * @var MinecraftPing
     */
    private $mc_query;
    /**
     * @var GameQ
     */
    private $gameQ;

    /**
     * GameQuery constructor.
     * @param MinecraftPing $mc_query
     */
    public function __construct(MinecraftPing $mc_query)
    {
        $this->mc_query = $mc_query;
        $this->gameQ = new GameQ();
    }


    /**
     * @param BaseServer $server
     * @return BaseServer
     */
    public function updateQuery($server)
    {
        if ($server==null) {
            return null;
        }
        //Testing jenkins gitlab integration
        //Test 2
        for ($i=0; $i<3; $i++) {
            switch (true) {
                case $server instanceof MinecraftServer:
                    $this->updateMC($server);
                    break;
                case $server instanceof MultiplayerServer:
                    $this->updateMultiplayer($server);
                    break;
            }
            if ($server->isOnline()) {
                break;
            }
        }
        return $server;
    }

    /**
     * @param MultiplayerServer $server
     * @return MultiplayerServer
     */
    public function updateMultiplayer($server)
    {
        try {
            $hostname = gethostbyname($server->getHostname()).':'.$server->getPort();
            if ($server instanceof KillingFloorServer && $server->getQueryPort()!=null) {
                $this->gameQ->addServer([
                    'type' => $server->getGameStatistics()->getGameType(),
                    'host' => $hostname,
                    'options' => ['query_port'=>$server->getQueryPort()]
                ])->addFilter('normalize')->setOption('timeout', 5);
            } else {
                $this->gameQ->addServer([
                    'type' => $server->getGameStatistics()->getGameType(),
                    'host' => $hostname
                ])->addFilter('normalize')->setOption('timeout', 5);
            }
            $rs = $this->gameQ->process();
        } catch (\Exception $e) {
            $server->update(null);
            return $server;
        }
        //print_r(json_encode($rs));
        if (!$rs[$hostname]['gq_online']) {
            $server->update(null);
        } else {
            $server->update($rs[$hostname]);
        }
        return $server;
    }

    /**
     * @param MinecraftServer $server
     * @return MinecraftServer
     */
    public function updateMC($server)
    {
        try {
            $rs = $this->mc_query->init($server->getHostname(), $server->getPort())->Query();
            $rs['gq_maxplayers'] = $rs["players"]["max"];
            $rs['gq_numplayers'] = $rs["players"]["online"];
            $server->update($rs);
        } catch (\Exception $e) {
            $server->update(null);
        }
        return $server;
    }
}
