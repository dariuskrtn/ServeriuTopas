<?php

/**
 * Created by PhpStorm.
 * User: darius0021
 * Date: 16.9.6
 * Time: 16.53
 */

namespace AppBundle\Services;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Type\BaseServer;
use AppBundle\Entity\Statistics\GameStatistics;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\DateTime;

class Utilities
{
    const HOURS_TO_REVOTE = 22;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * Utilities constructor.
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $ip
     * @param \AppBundle\Entity\Server\Type\BaseServer $server
     * @return integer
     */

    public function timeLeftToVote($ip, $server)
    {
        $vote = $this->em->getRepository('AppBundle:Vote')->getLatestVote($ip, $server);
        if ($vote==null) {
            return 0;
        }
        $old = $vote->getTime()->modify("+".self::HOURS_TO_REVOTE." hour");
        $new = new \DateTime;
        if ($old<$new) {
            return 0;
        }
        return $old->getTimestamp()-$new->getTimestamp();
    }
    /**
     * @param string $game
     * @return GameStatistics
     */
    public function getGameStatistics($game)
    {
        $stats = $this->em->getRepository('AppBundle:Statistics\GameStatistics')->findOneBy([
            'name' => $game
        ], []);
        if ($stats!=null) {
            return $stats;
        }
        $stats = new GameStatistics($game);
        return $stats;
    }
    /**
     * @param string $game
     * @param integer $id
     * @return BaseServer|null
     */
    public function getServer($game, $id)
    {
        return $this->em->getRepository("AppBundle:Server\\" . $game . "Server")->find($id);
    }
    /**
     * @param string $game
     * @param string $order
     * @return array|null
     */
    public function getServers($game, $order)
    {
        return $this->em->getRepository("AppBundle:Server\\" . $game . "Server")->findBy([], [$order => 'DESC']);
    }

    /**
     * @param string $game
     * @param integer $max
     * @return BaseServer[]
     */
    public function getServersByVotes($game, $max)
    {
        return $this->em->createQueryBuilder()->select('s')->from("AppBundle:Server\\" . $game . "Server", 's')
            ->leftJoin('s.rating', 'r')->orderBy('r.totalVotes', 'desc')->setMaxResults($max)->getQuery()->getResult();
    }
    /**
     * @param string $game
     * @param integer $max
     * @return BaseServer[]
     */
    public function getServersByPlayersOn($game, $max)
    {
        return $this->em->createQueryBuilder()->select('s')->from('AppBundle:Server\\'.$game.'Server', 's')
            ->orderBy('s.users', 'DESC')->setMaxResults($max)->getQuery()->getResult();
    }
    /**
     * @param User $user
     * @return array
     */
    public function getUserServers($user)
    {
        $games = $this->em->getRepository('AppBundle:Statistics\GameStatistics')->findAll();
        $servers = [];
        foreach ($games as $g) {
            $srv = $this->em->getRepository('AppBundle:Server\\'.$g->getName().'Server')
                ->findBy(['owner'=>$user->getId()], []);
            $servers = array_merge($servers, $srv);
        }
        return $servers;
    }

    /**
     * @param string $game
     * @param User $user
     * @param Rating $rating
     * @return BaseServer
     */
    public function createGameServer($game, $user, $rating)
    {
        $className = "AppBundle\\Entity\\Server\\".$game."Server";
        return new $className($user,$rating);
    }

    /**
     * @param string $game
     * @param int $time
     * @return BaseServer[]
     */
    public function getOldestUpdated($game, $time)
    {
        $expr = Criteria::expr();
        $criteria = Criteria::create();
        $now = new \DateTime();
        $now->modify("-".$time." minute");
        $criteria->where($expr->lte('updateTime', $now));
        $all = $this->em->getRepository("AppBundle:Server\\".$game."Server")->matching($criteria);

        if (count($all) == 0) {
            return null;
        }
        return $all;
    }
}
