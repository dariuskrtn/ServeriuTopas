<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServerPlayer
 * NOT IN DB
 */
class ServerPlayer implements \JsonSerializable
{
    private $name;
    private $score;

    /**
     * ServerPlayer constructor.
     * @param string $name
     * @param integer $score
     */
    public function __construct($name = null, $score = null)
    {
        $this->name = $name;
        $this->score = $score;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ServerPlayer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return ServerPlayer
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    public function jsonSerialize()
    {
        return [
         'name' => $this->getName(),
         'score' => $this->getScore()
        ];
    }
}
