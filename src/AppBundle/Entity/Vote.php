<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;

/**
 * Vote
 *
 * @ORM\Table(name="vote")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VoteRepository")
 */
class Vote implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $ipAddress;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $time;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Rating", inversedBy="votes")
     * @var Rating
     */
    private $rating;

    /**
     * @Recaptcha\IsTrue
     */
    public $recaptcha;

    /**
     * Vote constructor.
     * @param Rating $rating
     */
    public function __construct($rating)
    {
        $this->time = new \DateTime();
        $this->rating = $rating;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return Rating
     */
    public function getServer()
    {
        return $this->rating;
    }

    /**
     * @param Rating $rating
     */
    public function setServer($rating)
    {
        $this->rating = $rating;
    }
    public function jsonSerialize()
    {
        return [
            'ip' => $this->getIpAddress(),
            'time' => $this->getTime()
        ];
    }
}
