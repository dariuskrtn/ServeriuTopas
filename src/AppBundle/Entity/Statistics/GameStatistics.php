<?php

namespace AppBundle\Entity\Statistics;
/**
 * Created by PhpStorm.
 * User: darius0021
 * Date: 16.10.5
 * Time: 19.06
 */
use Doctrine\ORM\Mapping as ORM;
/**
 * Stats
 *
 * @ORM\Table(name="game_statistics")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameStatisticsRepository")
 */
class GameStatistics
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $gameName;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $gameType="";
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $totalServers=0;
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $totalVotes=0;
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $totalViews=0;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $description="";

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $trailer="";

    /**
     * GameStatistics constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->gameName = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getGameName()
    {
        return $this->gameName;
    }

    /**
     * @param string $gameName
     */
    public function setGameName($gameName)
    {
        $this->gameName = $gameName;
    }

    /**
     * @return string
     */
    public function getGameType()
    {
        return $this->gameType;
    }

    /**
     * @param string $gameType
     */
    public function setGameType($gameType)
    {
        $this->gameType = $gameType;
    }

    /**
     * @return int
     */
    public function getTotalServers()
    {
        return $this->totalServers;
    }

    /**
     * @param int $totalServers
     */
    public function setTotalServers($totalServers)
    {
        $this->totalServers = $totalServers;
    }

    /**
     * @return int
     */
    public function getTotalVotes()
    {
        return $this->totalVotes;
    }

    /**
     * @param int $totalVotes
     */
    public function setTotalVotes($totalVotes)
    {
        $this->totalVotes = $totalVotes;
    }

    /**
     * @return int
     */
    public function getTotalViews()
    {
        return $this->totalViews;
    }

    /**
     * @param int $totalViews
     */
    public function setTotalViews($totalViews)
    {
        $this->totalViews = $totalViews;
    }

    public function addTotalViews()
    {
        $this->totalViews++;
    }

    public function addTotalServers($plus)
    {
        if ($plus)
            $this->totalServers++;
        else
            $this->totalServers--;
    }

    public function addTotalVotes()
    {
        $this->totalVotes++;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTrailer()
    {
        return $this->trailer;
    }

    /**
     * @param string $trailer
     */
    public function setTrailer($trailer)
    {
        $this->trailer = $trailer;
    }

}