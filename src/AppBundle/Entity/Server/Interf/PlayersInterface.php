<?php

namespace AppBundle\Entity\Server\Interf;
/**
 * Created by PhpStorm.
 * User: darius0021
 * Date: 16.9.10
 * Time: 23.14
 */
interface PlayersInterface
{
    public function getPlayers();
    public function setPlayers($players);
}