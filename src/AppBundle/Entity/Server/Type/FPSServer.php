<?php

namespace AppBundle\Entity\Server\Type;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Interf\PlayersInterface;
use AppBundle\Entity\ServerPlayer;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class FPSServer
 * @ORM\MappedSuperclass
 */
class FPSServer extends MultiplayerServer implements PlayersInterface
{
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $map = "";
    /**
     * @ORM\Column(type="json_array", options={"collation":"utf8mb4_bin"})
     * @var array
     */
    private $players = [];
    /**
     * CounterStrikeServer constructor.
     * @param $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        parent::__construct($owner, $rating);
        $this->update(false);
    }

    /**
     * @return array
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @param array $players
     */
    public function setPlayers($players)
    {
        $this->players = $players;
    }

    /**
     * @return string
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @param string $map
     */
    public function setMap($map)
    {
        $this->map = $map;
    }

    /**
     * @param $data
     */
    public function update($data)
    {
        parent::update($data);
        if ($data==null) {
            $this->setMap("");
            $this->setPlayers([]);
        } else {
            $this->setMap($data['gq_mapname']);
            $pl = [];
            foreach ($data['players'] as $player) {
                $p = new ServerPlayer($player['gq_name'], $player['gq_score']);
                $pl[] = $p;
            }
            $this->setPlayers($pl);
        }
    }

    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'map' => $this->getMap(),
            'players' => $this->getPlayers()
        ]);
    }
}
