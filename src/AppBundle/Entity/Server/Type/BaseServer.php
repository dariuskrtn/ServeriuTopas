<?php

namespace AppBundle\Entity\Server\Type;

use AppBundle\Entity\Statistics\GameStatistics;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;
use AppBundle\Entity\Rating;
use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BaseServer
 * @ORM\MappedSuperclass
 */
class BaseServer implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="servers")
     * @var User
     */
    private $owner;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;
    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $description="";
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $createTime;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updateTime;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $webpage;
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Rating", mappedBy="server")
     * @ORM\JoinColumn(name="rating_id", referencedColumnName="id")
     * @var Rating
     */
    private $rating;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Statistics\GameStatistics")
     * @var GameStatistics
     */
    private $gameStatistics;
    /**
     * @Recaptcha\IsTrue
     */
    public $recaptcha;

    /**
     * BaseServer constructor.
     * @param User $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        $this->owner = $owner;
        $this->createTime = new \DateTime();
        $this->rating = $rating;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getCreateTime()
    {
        return $this->createTime;
    }

    /**
     * @param \DateTime $createTime
     */
    public function setCreateTime($createTime)
    {
        $this->createTime = $createTime;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * @param \DateTime $updateTime
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;
    }

    /**
     * @return string
     */
    public function getWebpage()
    {
        return $this->webpage;
    }

    /**
     * @param string $webpage
     */
    public function setWebpage($webpage)
    {
        if ($webpage!=null && strlen($webpage)>3) {
            if (substr($webpage, 0, 4)!="http") {
                $webpage = 'http://'.$webpage;
            }
        }
        $this->webpage = $webpage;
    }

    /**
     * @return Rating
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param Rating $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return GameStatistics
     */
    public function getGameStatistics()
    {
        return $this->gameStatistics;
    }

    /**
     * @param GameStatistics $gameStatistics
     */
    public function setGameStatistics($gameStatistics)
    {
        $this->gameStatistics = $gameStatistics;
    }

    /**
     * @return void
     * @param $data
     */
    public function update($data)
    {
        $this->setUpdateTime(new \DateTime());
    }
    
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'owner' => $this->getOwner()==null?'null':$this->getOwner()->getUsername(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'createTime' => $this->getCreateTime()->getTimestamp(),
            'updateTime' => $this->getUpdateTime()->getTimestamp(),
            'votes' => count($this->getRating()->getVotes()),
            'critics' => $this->getRating()->getAverageRating(),
            'game' => $this->getGameStatistics()->getName()
        ];
    }
}
