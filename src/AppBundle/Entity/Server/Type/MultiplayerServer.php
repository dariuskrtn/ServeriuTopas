<?php

namespace AppBundle\Entity\Server\Type;

use AppBundle\Entity\Rating;
use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class MultiplayerServer
 * @ORM\MappedSuperclass
 */
class MultiplayerServer extends BaseServer implements \JsonSerializable
{
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $hostname;
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $port;
    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $online = false;
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $users = 0;
    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $maxUsers = 0;

    /**
     * MultiplayerServer constructor.
     * @param User $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        parent::__construct($owner, $rating);
    }

    /**
     * @return string
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * @param string $hostname
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return boolean
     */
    public function isOnline()
    {
        return $this->online;
    }

    /**
     * @param boolean $online
     */
    public function setOnline($online)
    {
        $this->online = $online;
    }

    /**
     * @return int
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param int $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return int
     */
    public function getMaxUsers()
    {
        return $this->maxUsers;
    }

    /**
     * @param int $maxUsers
     */
    public function setMaxUsers($maxUsers)
    {
        $this->maxUsers = $maxUsers;
    }

    /**
     * @return void
     * @param $data
     */
    public function update($data)
    {
        parent::update($data);
        if ($data==null) {
            $this->setMaxUsers(0);
            $this->setUsers(0);
            $this->setOnline(false);
        } else {
            $this->setOnline(true);
            $this->setMaxUsers($data['gq_maxplayers']);
            $this->setUsers($data['gq_numplayers']);
        }
    }
    
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'hostname' => $this->getHostname(),
            'port' => $this->getPort(),
            'online' => $this->isOnline(),
            'users' => $this->getUsers(),
            'max' => $this->getMaxUsers()
        ]);
    }
}
