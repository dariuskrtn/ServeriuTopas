<?php

namespace AppBundle\Entity\Server;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Type\FPSServer;
use Doctrine\ORM\Mapping as ORM;

/**
 * CounterStrikeCZServer
 *
 * @ORM\Table(name="counter_strike_s_server")
 * @ORM\Entity()
 */
class CounterStrikeSServer extends FPSServer
{
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $style="";
    /**
     * CounterStrikeServer constructor.
     * @param $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        parent::__construct($owner, $rating);
        $this->update(null);
    }

    /**
     * @return string
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param string $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @param array $data
     */
    public function update($data)
    {
        parent::update($data);
    }

    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'style' => $this->getStyle()
        ]);
    }
}
