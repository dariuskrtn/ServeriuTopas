<?php

namespace AppBundle\Entity\Server;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Type\MultiplayerServer;
use Doctrine\ORM\Mapping as ORM;

/**
 * MinecraftServer
 *
 * @ORM\Table(name="minecraft_server")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MinecraftServerRepository")
 */
class MinecraftServer extends MultiplayerServer
{
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $version="";

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $style="";
    /**
     * MinecraftServer constructor.
     * @param $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        parent::__construct($owner, $rating);
        $this->update(false);
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param string $style
     */
    public function setStyle($style)
    {
        $this->style = $style;
    }

    /**
     * @param $data
     */
    public function update($data)
    {
        parent::update($data);
        if ($data==false) {
            $this->setOnline(false);
            $this->setUsers(0);
            $this->setMaxUsers(0);
        } else {
            $this->setOnline(true);
            $this->setUsers($data["players"]["online"]);
            $this->setMaxUsers($data["players"]["max"]);
        }
    }

    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'version' => $this->getVersion(),
            'style' => $this->getStyle()
        ]);
    }
}
