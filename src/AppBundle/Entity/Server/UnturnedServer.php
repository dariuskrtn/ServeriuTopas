<?php

namespace AppBundle\Entity\Server;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Type\MultiplayerServer;
use Doctrine\ORM\Mapping as ORM;

/**
 * UnturnedServer
 *
 * @ORM\Table(name="unturned_server")
 * @ORM\Entity()
 */
class UnturnedServer extends MultiplayerServer
{
    /**
     * UnturnedServer constructor.
     * @param $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        parent::__construct($owner, $rating);
        $this->update(null);
    }

    /**
     * @param array $data
     */
    public function update($data)
    {
        parent::update($data);
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize();
    }
}
