<?php

namespace AppBundle\Entity\Server;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Type\MultiplayerServer;
use Doctrine\ORM\Mapping as ORM;

/**
 * SAMPServer
 *
 * @ORM\Table(name="rust_server")
 * @ORM\Entity()
 */
class RustServer extends MultiplayerServer
{
    /**
     * CounterStrikeServer constructor.
     * @param $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        parent::__construct($owner, $rating);
        $this->update(null);
    }

    /**
     * @param array $data
     */
    public function update($data)
    {
        parent::update($data);
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize();
    }
}
