<?php

namespace AppBundle\Entity\Server;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Type\FPSServer;
use Doctrine\ORM\Mapping as ORM;

/**
 * KillingFloor2Server
 *
 * @ORM\Table(name="killing_floor_2_server")
 * @ORM\Entity()
 */
class KillingFloor2Server extends FPSServer
{
    /**
     * CounterStrikeServer constructor.
     * @param $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        parent::__construct($owner, $rating);
        $this->update(null);
    }

    /**
     * @param array $data
     */
    public function update($data)
    {
        parent::update($data);
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize();
    }
}
