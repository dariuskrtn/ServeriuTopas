<?php

namespace AppBundle\Entity\Server;

use AppBundle\Entity\Rating;
use AppBundle\Entity\Server\Type\FPSServer;
use Doctrine\ORM\Mapping as ORM;

/**
 * KillingFloorServer
 *
 * @ORM\Table(name="killing_floor_server")
 * @ORM\Entity()
 */
class KillingFloorServer extends FPSServer
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $queryPort;
    /**
     * CounterStrikeServer constructor.
     * @param $owner
     * @param Rating $rating
     */
    public function __construct($owner, $rating)
    {
        parent::__construct($owner, $rating);
        $this->update(null);
    }

    /**
     * @return int
     */
    public function getQueryPort()
    {
        return $this->queryPort;
    }

    /**
     * @param int $queryPort
     */
    public function setQueryPort($queryPort)
    {
        $this->queryPort = $queryPort;
    }

    /**
     * @param array $data
     */
    public function update($data)
    {
        parent::update($data);
    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize();
    }
}
