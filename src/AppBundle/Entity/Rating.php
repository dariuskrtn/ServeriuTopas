<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Server\Type\BaseServer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Rating
 *
 * @ORM\Table(name="rating")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RatingRepository")
 */
class Rating implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Server\Type\BaseServer", inversedBy="rating")
     * @var BaseServer
     */
    private $server;

    /**
     * @ORM\OneToMany(targetEntity="Vote", mappedBy="rating")
     * @var array
     */
    private $votes;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $totalVotes = 0;

    /**
     * @var ArrayCollection
     */
    private $ratings;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ratings")
     * @var User
     */
    private $whoRated;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return BaseServer
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param BaseServer $server
     */
    public function setServer($server)
    {
        $this->server = $server;
    }

    /**
     * @return array
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @param array $votes
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;
    }

    /**
     * @return ArrayCollection
     */
    public function getRatings()
    {
        return $this->ratings;
    }

    /**
     * @param ArrayCollection $ratings
     */
    public function setRatings($ratings)
    {
        $this->ratings = $ratings;
    }

    /**
     * @return User
     */
    public function getWhoRated()
    {
        return $this->whoRated;
    }

    /**
     * @param User $whoRated
     */
    public function setWhoRated($whoRated)
    {
        $this->whoRated = $whoRated;
    }

    /**
     * @return integer
     */
    public function getTotalVotes()
    {
        return $this->totalVotes;
    }

    /**
     * @param integer $totalVotes
     */
    public function setTotalVotes($totalVotes)
    {
        $this->totalVotes = $totalVotes;
    }


    public function getAverageRating()
    {
        if (!is_array($this->getRatings()) || count($this->getRatings())==0) {
            return 0;
        }
        $sum = 0;
        foreach ($this->getRatings() as $r) {
            $sum+=$r;
        }
        return $sum/count($this->getRatings());
    }
    public function jsonSerialize()
    {
        $votes = [];
        foreach ($this->getVotes() as $vote) {
            $v = [
                'ip' => $vote->getIpAddress(),
                'date' => $vote->getTime()
            ];
            $votes = $v;
        }
        return [
            'votes' => $votes,
            'ratings' => $this->getRatings(),
            'whoRated' => $this->getWhoRated()==null?'null':$this->getWhoRated()->getUsername()
        ];
    }
}
