/**
 * Created by darius0021 on 16.10.6.
 */
var app = angular.module("app", []).config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}')
});

app.controller('Ctrl', function($scope, $filter, $http, $window) {
    $scope.loaded = false;
    $scope.servers = [];
    $scope.votes = -1;
    $scope.game = "";

    $scope.getServers = function($game) {

        $http.get('/json/'+$game+'/servers/', {
        }).success(function(data) {
            if (data.length >= 1) {
                $scope.servers = data;
                $scope.checkOld($game);
            }
            $scope.loaded = true;
        });
    };
    $scope.getPlayerServers = function($id) {

        $http.get('/json/'+$id+'/userServers/', {
        }).success(function(data) {
            if (data.length >= 1) {
                $scope.servers = data;
            }
            $scope.loaded = true;
        });
    };
    $scope.checkOld = function($game) {

        $currTime = Math.floor(Date.now() / 1000);
        angular.forEach($scope.servers, function(value, key) {
            if ($currTime-value['updateTime']>120) {
                $scope.updateOld($game, value['id'], key);
            }
        });
    };
    $scope.updateOld = function ($game, $id, $key) {
        $http.get('/json/'+$game+'/server/'+$id+'/?update=true', {
        }).success(function(data) {
            $scope.servers[$key] = data;
        });
    };
    $scope.removeServer = function ($game, $id, index) {
        if (confirm("Ar tikrai norite ištrinti šį serverį?\nIštrynus grąžinti bus neįmanoma.") == true) {
            $http.post('/json/'+$game+'/remove/'+$id+'/').success(function (data) {
                if (data == 'OK')
                    $scope.servers.splice(index, 1);
            });
        }
    };
    $scope.getTimeUpdated = function ($ts) {
        var $curr = Date.now()/1000 | 0;
        var $timePassed = ($curr - $ts); // Sekundes (+60) fix'as kazkur dingusios vienos minutes..
        var $min = $timePassed/60 | 0;
        var $sec = $timePassed-$min*60;
        if ($min>0) {
            return $min + ' min. ' + $sec + ' sek.';
        } else {
            return $sec + ' sek.';
        }
    };
    $scope.getVotes = function ($game, $id) {
        $http.get('/json/'+$game+'/votes/'+$id+'/', {
        }).success(function(data) {
            $scope.votes = data;
        });
    };
    $scope.specialLink2 = function (game, id) {
        if (!window.getSelection()) return;
        if (window.getSelection().anchorOffset==window.getSelection().focusOffset)
            if (game==null)
            window.location.href = id+"/";
            else
                window.location.href = "/servers/"+game+"/"+id+"/";
    }
});

function specialLink(link) {
    if (!window.getSelection()) return;
    if (window.getSelection().anchorOffset==window.getSelection().focusOffset)
        window.location.href = link;
}